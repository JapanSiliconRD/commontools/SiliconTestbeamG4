# Software package for Silicon R&D
## Author Koji Nakamura Koji.Nakamura@cern.ch
####   first created on 29th Jul 2021

## [INSTALL]

### before install this software :
#### setup g++ >=4.8
#### setup ROOT (root 6 is recommended)
#### setup Geant4 >10.5 (geant4 10.7 is recommended)
#### example at atlaspc7.kek.jp
    mkdir ~/work/Geant4
    cd ~/work/Geant4
    git clone https://:@gitlab.cern.ch:8443/kojin/SiliconTestbeamG4.git SiliconTestbeamG4



## [COMPILE]

    source /home/software/geant4/v10.07/bin/geant4.sh
    cd SiliconTestbeamG4/TestbeamSetup
    mkdir build
    cd build
    cmake3 -DCMAKE_INSTALL_PREFIX=../../ ..
    make
    make install

#### login again and use this package second time you can only do : 
#### (make may not need in case you haven't change any codes)

    source /home/software/geant4/v10.07/bin/geant4.sh
    cd ~/work/Geant4/SiliconTestbeamG4/TestbeamSetup/build
    make
    make install



## [EXECUTE program]
    cd ../../run/
    ../bin/silicontestbeam -m elph800MeV.mac
