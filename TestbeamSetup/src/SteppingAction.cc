/// \file SteppingAction.cc
/// \brief Implementation of the SteppingAction class

#include "SteppingAction.hh"
#include "EventAction.hh"
#include "DetectorConstruction.hh"

#include "G4Step.hh"
#include "G4RunManager.hh"
#include "Analysis.hh"
#include "G4Event.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SteppingAction::SteppingAction(
                      DetectorConstruction* det,
                      EventAction* event)
  : G4UserSteppingAction()
   ,    fEventAction(event)
{
  fDetConstruction=det;
  fei4tel0= det->Getfei4tel0();
  fei4tel1= det->Getfei4tel1();
  fei4tel2= det->Getfei4tel2();
  fei4tel3= det->Getfei4tel3();
  fei4roi= det->Getfei4roi();
  lgad0= det->Getlgad0();
  lgad1= det->Getlgad1();
}
//                      const DetectorConstruction* detectorConstruction,
//                      EventAction* eventAction)
//  : G4UserSteppingAction(),
//    fDetConstruction(detectorConstruction),
//    fEventAction(eventAction)
//{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SteppingAction::~SteppingAction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void SteppingAction::UserSteppingAction(const G4Step* step)
{
// Collect energy and track length step by step
  auto chg= step->GetTrack()->GetDefinition()->GetPDGCharge() ;
  if(chg!=0) {
// get volume of the current step
     auto volume = step->GetPreStepPoint()->GetTouchableHandle()->GetVolume();
     auto edep       = step->GetTotalEnergyDeposit();
     auto thetime    = step->GetPreStepPoint()->GetGlobalTime();
     auto stepLength = step->GetStepLength();
     //     auto analysisManager = G4AnalysisManager::Instance();
     // Test of local coordinate
     //From   BookForAppliDev.pdf   Page 283.
     //G4StepPoint* preStepPoint = step->GetPreStepPoint();
     //G4TouchableHandle theTouchable = preStepPoint->GetTouchableHandle();
     //G4ThreeVector worldPosition = preStepPoint->GetPosition();
     //G4ThreeVector localPosition = theTouchable->GetHistory()-> GetTopTransform().TransformPoint(worldPosition);


     G4StepPoint* p0 = step -> GetPreStepPoint();
     G4TouchableHandle t0 = p0 -> GetTouchableHandle();
     G4ThreeVector wp0 = p0 -> GetPosition();
     G4ThreeVector lp0 = t0-> GetHistory()-> GetTopTransform().TransformPoint(wp0);
     G4StepPoint* p1 = step -> GetPostStepPoint();
     G4ThreeVector wp1 = p1 -> GetPosition();
     G4ThreeVector lp1 = t0-> GetHistory()-> GetTopTransform().TransformPoint(wp1);
     if(fEventAction -> GetBeamPosZ0() <-5000.0*mm) {
       G4ThreeVector mom = p0 -> GetMomentum();
       fEventAction -> SetBeaminfo(wp0.getX(),wp0.getY(),wp0.getZ(),mom.getX(),mom.getY(),mom.getZ());
     }
     if(0) G4cout << " p0(X Y Z)mm " << lp0.getX()/mm << " / "<< lp0.getY()/mm << " / "<< lp0.getZ()/mm 
            << " p1(X Y Z)mm " << lp1.getX()/mm << " / "<< lp1.getY()/mm << " / "<< lp1.getZ()/mm  <<G4endl;

     if ( volume == fei4tel0->GetCmosPV() ) {
       fei4tel0->AddCmos(edep,stepLength,thetime,0,0);
     } else if ( volume == fei4tel0->GetDeplPV() ) {
       G4int ix = step->GetPreStepPoint()->GetTouchableHandle()->GetReplicaNumber(1);
       G4int iy = step->GetPreStepPoint()->GetTouchableHandle()->GetReplicaNumber(2);
       fei4tel0->AddDepl(edep,stepLength,thetime,iy,ix,lp0,lp1);
     } else if ( volume == fei4tel0->GetWaferPV() ) {
       fei4tel0->AddWafer(edep,stepLength,thetime,0,0);
     } else if ( volume == fei4tel1->GetCmosPV() ) {
       fei4tel1->AddCmos(edep,stepLength,thetime,0,0);
     } else if ( volume == fei4tel1->GetDeplPV() ) {
       G4int ix = step->GetPreStepPoint()->GetTouchableHandle()->GetReplicaNumber(1);
       G4int iy = step->GetPreStepPoint()->GetTouchableHandle()->GetReplicaNumber(2);
       fei4tel1->AddDepl(edep,stepLength,thetime,iy,ix,lp0,lp1);
     } else if ( volume == fei4tel1->GetWaferPV() ) {
       fei4tel1->AddWafer(edep,stepLength,thetime,0,0);
     } else if ( volume == fei4tel2->GetCmosPV() ) {
       fei4tel2->AddCmos(edep,stepLength,thetime,0,0);
     } else if ( volume == fei4tel2->GetDeplPV() ) {
       G4int ix = step->GetPreStepPoint()->GetTouchableHandle()->GetReplicaNumber(1);
       G4int iy = step->GetPreStepPoint()->GetTouchableHandle()->GetReplicaNumber(2);
       fei4tel2->AddDepl(edep,stepLength,thetime,iy,ix,lp0,lp1);
     } else if ( volume == fei4tel2->GetWaferPV() ) {
       fei4tel2->AddWafer(edep,stepLength,thetime,0,0);
     } else if ( volume == fei4tel3->GetCmosPV() ) {
       fei4tel3->AddCmos(edep,stepLength,thetime,0,0);
     } else if ( volume == fei4tel3->GetDeplPV() ) {
       G4int ix = step->GetPreStepPoint()->GetTouchableHandle()->GetReplicaNumber(1);
       G4int iy = step->GetPreStepPoint()->GetTouchableHandle()->GetReplicaNumber(2);
       fei4tel3->AddDepl(edep,stepLength,thetime,iy,ix,lp0,lp1);
     } else if ( volume == fei4roi->GetCmosPV() ) {
       fei4roi->AddCmos(edep,stepLength,thetime,0,0);
     } else if ( volume == fei4roi->GetDeplPV() ) {
       G4int ix = step->GetPreStepPoint()->GetTouchableHandle()->GetReplicaNumber(1);
       G4int iy = step->GetPreStepPoint()->GetTouchableHandle()->GetReplicaNumber(2);
       fei4roi->AddDepl(edep,stepLength,thetime,iy,ix,lp0,lp1);
     } else if ( volume == lgad0->GetCmosPV() ) {
       lgad0->AddCmos(edep,stepLength,thetime,0,0);
     } else if ( volume == lgad0->GetDeplPV() ) {
       G4int ix = step->GetPreStepPoint()->GetTouchableHandle()->GetReplicaNumber(1);
       G4int iy = step->GetPreStepPoint()->GetTouchableHandle()->GetReplicaNumber(2);
       lgad0->AddDepl(edep,stepLength,thetime,iy,ix,lp0,lp1);
     } else if ( volume == lgad0->GetWaferPV() ) {
       lgad0->AddWafer(edep,stepLength,thetime,0,0);
     } else if ( volume == lgad1->GetCmosPV() ) {
       lgad1->AddCmos(edep,stepLength,thetime,0,0);
     } else if ( volume == lgad1->GetDeplPV() ) {
       G4int ix = step->GetPreStepPoint()->GetTouchableHandle()->GetReplicaNumber(1);
       G4int iy = step->GetPreStepPoint()->GetTouchableHandle()->GetReplicaNumber(2);
       lgad1->AddDepl(edep,stepLength,thetime,iy,ix,lp0,lp1);
     } else if ( volume == lgad1->GetWaferPV() ) {
       lgad1->AddWafer(edep,stepLength,thetime,0,0);
     }
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
