/// \file DetectorConstruction.cc
/// \brief Implementation of the DetectorConstruction class

#include "G4Material.hh"
#include "G4NistManager.hh"

#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PVReplica.hh"
#include "G4GlobalMagFieldMessenger.hh"
#include "G4AutoDelete.hh"

#include "G4GeometryManager.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4SolidStore.hh"

#include "G4VisAttributes.hh"
#include "G4Colour.hh"

#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"

#include "DetectorConstruction.hh"


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4ThreadLocal 
G4GlobalMagFieldMessenger* DetectorConstruction::fMagFieldMessenger = nullptr; 

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::DetectorConstruction()
 : G4VUserDetectorConstruction()
{
  fei4tel0    = new Pixel(10, 1344,  40,  0,  25.0*um, 500.0*um,  3*um, 150*um, 150*um, 10*um);
  fei4tel1    = new Pixel(20,   40,1344,  0, 500.0*um,  25.0*um,  3*um, 150*um, 150*um, 10*um);
  lgad0       = new Pixel(30,    2,   2,  0, 500.0*um, 500.0*um,  3*um,  10*um,  50*um,150*um);
  lgad1       = new Pixel(40,    1,  16,  0,9000.0*um,  80.0*um,  3*um,  10*um,  50*um,150*um);
  fei4tel2    = new Pixel(50,   40,1344,  0, 500.0*um,  25.0*um,  3*um, 150*um, 150*um, 10*um);
  fei4tel3    = new Pixel(60, 1344,  40,  0,  25.0*um, 500.0*um,  3*um, 150*um, 150*um, 10*um);
  fei4roi     = new Pixel(70,  160, 672,  0,  50.0*um, 250.0*um,  3*um, 150*um, 150*um, 10*um);

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::~DetectorConstruction()
{ 
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* DetectorConstruction::Construct()
{
  // Define materials 
  DefineMaterials();
  
  // Define volumes
  return DefineVolumes();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void DetectorConstruction::DefineMaterials()
{ 
  /*
  // Lead material defined using NIST Manager
  auto nistManager = G4NistManager::Instance();
  nistManager->FindOrBuildMaterial("G4_Pb");
  
  // Liquid argon material
  G4double a;  // mass of a mole;
  G4double z;  // z=mean number of protons;  
  G4double density; 
  new G4Material("liquidArgon", z=18., a= 39.95*g/mole, density= 1.390*g/cm3);
         // The argon by NIST Manager is a gas with a different density

  // Vacuum
  new G4Material("Galactic", z=1., a=1.01*g/mole,density= universe_mean_density,
                  kStateGas, 2.73*kelvin, 3.e-18*pascal);
  */
  
  G4double a;  // mass of a mole;
  G4double z;  // z=mean number of protons;  
  G4double density; 
  G4String symbol;
  G4int ncomponents, natoms;
  //new G4Material("liquidArgon", z=18., a= 39.95*g/mole, density= 1.390*g/cm3);
  // The argon by NIST Manager is a gas with a different density
  new G4Material("Iron", z=26., a= 56*g/mole, density= 7.9*g/cm3);
  
  // Vacuum
  new G4Material("Galactic", z=1., a=1.01*g/mole,density= universe_mean_density,
		 kStateGas, 2.73*kelvin, 3.e-18*pascal);
  
  // new G4Material("Aluminium", z=13., a=26.98*g/mole, density=2.700*g/cm3);
  // new G4Material("Nitrogen", z=7.0,  a=14*g/mole, density=0.1*g/cm3);
  
  G4Element* Alu = new G4Element("Aluminum",symbol="Al" , z= 13., a= 26.9*g/mole);
  G4Element* Nit = new G4Element("Nitrogen",symbol="N" , z= 7., a= 14.0*g/mole);
  G4Element* Oxy = new G4Element("Oxygen",symbol="O" , z= 8., a= 16*g/mole);
  G4Material* AlN = new G4Material("AlN",density= 3.200*g/cm3, ncomponents=2);
  AlN->AddElement(Alu, natoms=1);
  AlN->AddElement(Nit , natoms=1);
  G4Material* Al2O3 = new G4Material("Al2O3",density= 3.800*g/cm3, ncomponents=2);
  Al2O3->AddElement(Alu, natoms=2);
  Al2O3->AddElement(Oxy , natoms=3);
  
  // Print materials
  G4cout << *(G4Material::GetMaterialTable()) << G4endl;
}
						 
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
						 
G4VPhysicalVolume* DetectorConstruction::DefineVolumes()
{

  // Get pointer to 'Material Manager'
  G4NistManager* materi_Man = G4NistManager::Instance();
  
  // Define 'World Volume'
  // Define the shape of solid
  G4double leng_X_World = 0.2*m;         // X-full-length of world
  G4double leng_Y_World = 0.2*m;         // Y-full-length of world
  G4double leng_Z_World = 0.5*m;         // Z-full-length of world
  G4Box* solid_World =
    new G4Box("Solid_World", leng_X_World/2.0, leng_Y_World/2.0, leng_Z_World/2.0);
  
  // Define logical volume
  G4Material* materi_World = materi_Man->FindOrBuildMaterial("G4_AIR");
  G4LogicalVolume* logVol_World =
    new G4LogicalVolume(solid_World, materi_World, "LogVol_World");
logVol_World->SetVisAttributes (G4VisAttributes::Invisible);
  
  // Placement of logical volume
  G4int copyNum_World = 0;               // Set ID number of world
  G4PVPlacement* physVol_World  =
	    new G4PVPlacement(G4Transform3D(), "PhysVol_World", logVol_World, 0, false, copyNum_World);
  
 G4LogicalVolume *lV_fei4tel0  = fei4tel0   ->Getlogvol();
 G4LogicalVolume *lV_fei4tel1  = fei4tel1   ->Getlogvol();
 G4LogicalVolume *lV_fei4tel2  = fei4tel2   ->Getlogvol();
 G4LogicalVolume *lV_fei4tel3  = fei4tel3   ->Getlogvol();
 G4LogicalVolume *lV_fei4roi   = fei4roi    ->Getlogvol();
 G4LogicalVolume *lV_lgad0= lgad0 ->Getlogvol();
 G4LogicalVolume *lV_lgad1= lgad1 ->Getlogvol();

  
  // Placement of the pixels to the world: Put the 'global envelop'

  G4double pos_X, pos_Y, pos_Z;
  G4ThreeVector vec;
  G4RotationMatrix rot;
  G4Transform3D trans;
  G4int cN;

  pos_X = 0.000*mm;
  pos_Y = 0.000*mm;
  pos_Z = 0.0*mm;
  vec = G4ThreeVector(pos_X, pos_Y, pos_Z);
  rot = G4RotationMatrix();
  trans = G4Transform3D(rot, vec);
  cN = 1000;    
new G4PVPlacement(trans, "PV_fei4tel0", lV_fei4tel0, physVol_World,false, cN);

pos_X =  0.0*mm;
pos_Y =  0.0*mm;
pos_Z = 30.0*mm;
vec = G4ThreeVector(pos_X, pos_Y, pos_Z);
rot =  G4RotationMatrix();
//rot.rotateZ(0.4*deg);
trans = G4Transform3D(rot, vec);
cN=cN+10;
new G4PVPlacement(trans, "PV_fei4tel1", lV_fei4tel1, physVol_World,false, cN);

pos_X = 0.0*cm;
pos_Y = 0.0*cm;
pos_Z = 60*mm;
vec = G4ThreeVector(pos_X, pos_Y, pos_Z);
rot = G4RotationMatrix();
//rot.rotateY(1*deg);
trans = G4Transform3D(rot, vec);
 cN=cN+10;
 new G4PVPlacement(trans, "PV_lgad0", lV_lgad0, physVol_World,false, cN);

pos_X = 0.0*cm;
pos_Y = 0.0*cm;
pos_Z = 90*mm;
vec = G4ThreeVector(pos_X, pos_Y, pos_Z);
rot = G4RotationMatrix();
//rot.rotateZ(0.5*deg);
trans = G4Transform3D(rot, vec);
 cN=cN+10;
new G4PVPlacement(trans, "PV_lgad1", lV_lgad1, physVol_World,false, cN);

// Kyocera LSI package package:  Default material is Alumina ( A440 or A445 )
G4Box* CeraPKG= new G4Box("CeraPKG", 1.0*mm , 1.0*mm, 1.27*mm/2.0);
#if   0
G4LogicalVolume* lV_CeraPKG = new G4LogicalVolume(CeraPKG,materi_World, "CeraPKG");
#elif 0
G4LogicalVolume* lV_CeraPKG = new G4LogicalVolume(CeraPKG,materi_Man->FindOrBuildMaterial("AlN"), "CeraPKG");
#elif 0
G4LogicalVolume* lV_CeraPKG = new G4LogicalVolume(CeraPKG,materi_Man->FindOrBuildMaterial("Iron"), "CeraPKG");
#else
G4LogicalVolume* lV_CeraPKG = new G4LogicalVolume(CeraPKG,materi_Man->FindOrBuildMaterial("Al2O3"), "CeraPKG");
#endif
pos_X = 0.0*cm;
pos_Y = 0.0*cm;
pos_Z = 93*mm;
vec = G4ThreeVector(pos_X, pos_Y, pos_Z);
rot = G4RotationMatrix();
trans = G4Transform3D(rot, vec);
 cN=cN+10;
new G4PVPlacement(trans, "PV_CeraPKG", lV_CeraPKG, physVol_World,false, cN);



pos_X =  -0.00*mm;
pos_Y =  0.0*mm;
pos_Z = 120.0*mm;
vec = G4ThreeVector(pos_X, pos_Y, pos_Z);
rot =  G4RotationMatrix();
//rot.rotateZ(-1.2*deg);
trans = G4Transform3D(rot, vec);
cN=cN+10;
new G4PVPlacement(trans, "PV_fei4tel2", lV_fei4tel2, physVol_World,false, cN);

pos_X =  0.0*mm;
pos_Y =  0.0*mm;
pos_Z = 150.0*mm;
vec = G4ThreeVector(pos_X, pos_Y, pos_Z);
rot =  G4RotationMatrix();
//rot.rotateX(45.*deg);
trans = G4Transform3D(rot, vec);
cN=cN+10;
new G4PVPlacement(trans, "PV_fei4tel3", lV_fei4tel3, physVol_World,false, cN);

pos_X =  0.0*mm;
pos_Y =  0.0*mm;
pos_Z = 170.0*mm;
vec = G4ThreeVector(pos_X, pos_Y, pos_Z);
rot =  G4RotationMatrix();
//rot.rotateX(45.*deg);
trans = G4Transform3D(rot, vec);
cN=cN+10;
new G4PVPlacement(trans, "PV_fei4roi", lV_fei4roi, physVol_World,false, cN);


  //
  // Always return the physical World
  //
  return physVol_World;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void DetectorConstruction::ConstructSDandField()
{ 
  // Create global magnetic field messenger.
  // Uniform magnetic field is then created automatically if
  // the field value is not zero.
  G4ThreeVector fieldValue;
  fMagFieldMessenger = new G4GlobalMagFieldMessenger(fieldValue);
  fMagFieldMessenger->SetVerboseLevel(1);
  
  // Register the field messenger for deleting
  G4AutoDelete::Register(fMagFieldMessenger);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
