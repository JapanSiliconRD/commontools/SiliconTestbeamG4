/// \file DetectorConstruction.hh
/// \brief Definition of the DetectorConstruction class

#ifndef DetectorConstruction_h
#define DetectorConstruction_h 1

#include "G4PhysicalConstants.hh" 
#include "G4SystemOfUnits.hh"
#include "G4VUserDetectorConstruction.hh"
#include "globals.hh"
#include "Pixel.hh"

class G4VPhysicalVolume;
class G4GlobalMagFieldMessenger;

/// Detector construction class to define materials and geometry.
/// The calorimeter is a box made of a given number of layers. A layer consists
/// of an absorber plate and of a detection gap. The layer is replicated.
///
/// Four parameters define the geometry of the calorimeter :
///
/// - the thickness of an absorber plate,
/// - the thickness of a gap,
/// - the number of layers,
/// - the transverse size of the calorimeter (the input face is a square).
///
/// In addition a transverse uniform magnetic field is defined 
/// via G4GlobalMagFieldMessenger class.

class DetectorConstruction : public G4VUserDetectorConstruction
{
private: 
  Pixel *fei4tel0;
  Pixel *fei4tel1;
  Pixel *fei4tel2;
  Pixel *fei4tel3;
  Pixel *fei4roi;
  Pixel *lgad0;
  Pixel *lgad1;
  void DefineMaterials();
  G4VPhysicalVolume* DefineVolumes();
  static G4ThreadLocal G4GlobalMagFieldMessenger*  fMagFieldMessenger; 
  // magnetic field messenger
  
public:
  DetectorConstruction();
  virtual ~DetectorConstruction();
  
public:
  virtual G4VPhysicalVolume* Construct();
  virtual void ConstructSDandField();
  
  Pixel* Getfei4tel0() const { return fei4tel0;}
  Pixel* Getfei4tel1() const { return fei4tel1;}
  Pixel* Getfei4tel2() const { return fei4tel2;}
  Pixel* Getfei4tel3() const { return fei4tel3;}
  Pixel* Getfei4roi() const { return fei4roi;}
  Pixel* Getlgad0() const {return lgad0;}
  Pixel* Getlgad1() const {return lgad1;}

};


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

